package restfulCliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.io.OutputStream;

public class restfulCliente {
	
    public static void main(String[] args) throws IOException {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");
        //hola

    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    	String input = "";

        boolean seguir = true;
        while(seguir) {
        	System.out.println("**************************************************************");
        	System.out.println("Opciones:  1- listar personas 2- Listar asignaturas de persona 3-Crear Asignatura 4-Crear persona");
        	System.out.print("Elija una opcion > ");
            
            input = br.readLine();            	
        	

        	if(input.trim().compareTo("1") ==0 ) {
        	  	  try {

        		  		URL url = new URL("http://localhost:8180/personas/rest/personas/");
        		  		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        		  		conn.setRequestMethod("GET");
        		  		conn.setRequestProperty("Accept", "application/json");
        		  		//ads asd
        		
        		  		if (conn.getResponseCode() != 200) {
        		  			throw new RuntimeException("Failed : HTTP error code : "
        		  					+ conn.getResponseCode());
        		  		}
        		
        		  		BufferedReader bre = new BufferedReader(new InputStreamReader(
        		  			(conn.getInputStream())));
        		
        		  		String output;
        		  		System.out.println("Output from Server .... ");
        		  		System.out.println("Imprimiendo personas del sistema:");
        		  		while ((output = bre.readLine()) != null) {
        		  			System.out.println(output);
        		  			System.out.println();
        		  			
        		  		}

        		  		conn.disconnect();

        	  	  } catch (MalformedURLException e) {

        	  		e.printStackTrace();

        	  	  } catch (IOException e) {

        	  		e.printStackTrace();

        	  	  }
        		
        	}else if(input.trim().compareTo("2") ==0 ) {
        		
        		System.out.println("Introduzca el numero de cedula de la persona:");
        		
            	BufferedReader buffered = new BufferedReader(new InputStreamReader(System.in));
            	String entrada = "";
            	
            	entrada = buffered.readLine(); 
        		
        		
        		System.out.println("Se despliega en formato JSON el numero de cedula de la persona junto con el codigo de la asignatura en la que esta");
        		
        		System.out.println(entrada.trim()); 
        		
        		try {

    		  		URL url = new URL("http://localhost:8180/personas/rest/asignatura-persona/asignaturas_de_persona/"+entrada.trim());
    		  		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		  		conn.setRequestMethod("GET");
    		  		conn.setRequestProperty("Accept", "application/json");
    		  		//ads asd
    		
    		  		if (conn.getResponseCode() != 200) {
    		  			throw new RuntimeException("Failed : HTTP error code : "
    		  					+ conn.getResponseCode());
    		  		}
    		
    		  		BufferedReader bre = new BufferedReader(new InputStreamReader(
    		  			(conn.getInputStream())));
    		
    		  		String output;
    		  		System.out.println("Output from Server .... ");
    		  		System.out.println("Imprimiendo personas del sistema:");
    		  		while ((output = bre.readLine()) != null) {
    		  			System.out.println(output);
    		  			System.out.println();
    		  			
    		  		}

    		  		conn.disconnect();

    	  	  } catch (MalformedURLException e) {

    	  		e.printStackTrace();

    	  	  } catch (IOException e) {

    	  		e.printStackTrace();

    	  	  }
        		
        		
        	}else if(input.trim().compareTo("3") ==0 ) {
        		try {

        			URL url = new URL("http://localhostlocalhost:8180/personas/rest/asignatura-persona");
        			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        			conn.setDoOutput(true);
        			conn.setRequestMethod("POST");
        			conn.setRequestProperty("Content-Type", "application/json");
        			System.out.println("ingrese la asignatura:");
        			BufferedReader buffered = new BufferedReader(new InputStreamReader(System.in));
                	String asignatura = "";
                	
                	asignatura = buffered.readLine(); 

        			OutputStream os = conn.getOutputStream();
        			os.write(asignatura.getBytes());
        			os.flush();

        			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
        				throw new RuntimeException("Failed : HTTP error code : "
        					+ conn.getResponseCode());
        			}

        			BufferedReader bur = new BufferedReader(new InputStreamReader(
        					(conn.getInputStream())));

        			String output;
        			System.out.println("Output from Server .... \n");
        			while ((output = bur.readLine()) != null) {
        				System.out.println(output);
        			}

        			conn.disconnect();

        		  } catch (MalformedURLException e) {

        			e.printStackTrace();

        		  } catch (IOException e) {

        			e.printStackTrace();

        		 }

        		
        	}else{
        		seguir = false;
        	}
        }
        


  	}

  }
  
